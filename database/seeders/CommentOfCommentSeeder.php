<?php

namespace Database\Seeders;

use App\Models\CommentOfComment;
use Illuminate\Database\Seeder;

class CommentOfCommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CommentOfComment::factory()->times(8)->create();
    }
}