<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
        CategorySeeder::class,
        CommentOfCommentSeeder::class,
        CommentSeeder::class,
        ImageSeeder::class,
        LikeSeeder::class,
        PostSeeder::class,
        StatusSeeder::class,
        SubCategorySeeder::class,
        TagSeeder::class,
        VideoSeeder::class
    ]);
    }
}