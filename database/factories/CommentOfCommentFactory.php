<?php

namespace Database\Factories;

use App\Models\CommentOfComment;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommentOfCommentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CommentOfComment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'comment'=> $this->faker->realText(50),
        ];
    }
}