<?php

namespace Database\Factories;

use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title'=>$this->faker->word(1),
            'content'=>$this->faker->slug(20),
            'description'=> $this->faker->realText(500),
            'published_at' => $this->faker->dateTimeThisYear,
            'post_link' =>$this->faker->imageUrl(),
            'likes'=>$this->faker->randomDigitNotNull,
            'comments'=>$this->faker->randomDigitNotNull
        ];
    }
}