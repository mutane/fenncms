<?php

namespace App\Http\Controllers;

use App\Models\CommentOfComment;
use Illuminate\Http\Request;

class CommentOfCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CommentOfComment  $commentOfComment
     * @return \Illuminate\Http\Response
     */
    public function show(CommentOfComment $commentOfComment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CommentOfComment  $commentOfComment
     * @return \Illuminate\Http\Response
     */
    public function edit(CommentOfComment $commentOfComment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CommentOfComment  $commentOfComment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CommentOfComment $commentOfComment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CommentOfComment  $commentOfComment
     * @return \Illuminate\Http\Response
     */
    public function destroy(CommentOfComment $commentOfComment)
    {
        //
    }
}
