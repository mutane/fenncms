<?php

namespace App\Actions\Fortify;

use App\Models\Team;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),
        ])->validate();

        return DB::transaction(function () use ($input) {
            return tap(User::create([
                'name' => $input['name'],
                'email' => $input['email'],
                'password' => Hash::make($input['password']),
            ]), function (User $user) {
                if(count(\App\Models\Team::all())<=0){
                    $this->createTeam($user);
                }else{
                    $team = \App\Models\Team::where('user_id',1)->get()->first();
                 //   $tempUser = \App\Models\User::find(1)->first();
                    $user->teams()->attach($team->id,['role'=>'guest']);
                    \App\Models\User::where('id',$user->id)->update(['current_team_id'=>$team->id]);
                   // dd($tempUser);
                };

            });

        });
    }

    /**
     * Create a personal team for the user.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    protected function createTeam(User $user)
    {
        $user->ownedTeams()->save(Team::forceCreate([
            'user_id' => $user->id,
            'name' => "Blog",
            'personal_team' => false,
            //'current_team_id' =>,
        ]));
        $team = \App\Models\Team::where('user_id',1)->get()->first();
        $user->teams()->attach($team,['role'=>'admin']);
        \App\Models\User::where('id',$user->id)->update(['current_team_id'=>$team->id]);

    }

}